FROM node:erbium
MAINTAINER sms67@cam.ac.uk

RUN mkdir /protractor

WORKDIR /protractor

RUN npm install -g protractor@5.4.3 && \
    npm install -g protractor-jasmine2-html-reporter && \
    apt-get update && \
    yes | apt-get install default-jre

ENV NODE_PATH=/usr/local/lib/node_modules:/protractor/node_modules

# For the brave (build with the latest google chrome - need to instal latest protractor?:
# RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

COPY google-chrome-stable_current_amd64.deb ./
RUN yes | apt install ./google-chrome-stable_current_amd64.deb

# update webdriver after chrome install ?must be after?
RUN webdriver-manager update

COPY wait-for-it.sh /

ENTRYPOINT ["protractor"]