# README #

Container hosting Protractor environment

### Purpose ###

* This container provides an environment to allow more modern protractor tests (@5.4.3) to run and utilize a headless chrome instance

### Getting set up ###

* The protractor config.js requires the settings:
```
capabilities: {
    'browserName': process.env.PROT_BROWSER || 'chrome',
    // options required for headless docker usage
    chromeOptions: {
      args: ["--headless", "--no-sandbox", "--disable-gpu", "--window-size=1200x800"]
    }
  },
```

### To run ###

Change to the directory containing you spec directory and config.js file.
Execute the container

```
cd <your directory containing the tests>
docker run -it --privileged --rm --net=host -v /dev/shm:/dev/shm -v $(pwd):/protractor stesho-protractor config.js
```
